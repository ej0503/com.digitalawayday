package com.digitalawayday.model;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class ActivityTest {

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(Activity.class).verify();
    }

}
