package com.digitalawayday.model;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class TeamDayPlanTest {

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(TeamDayPlan.class).withOnlyTheseFields("beforeLunch", "afterLunch").verify();
    }

}
