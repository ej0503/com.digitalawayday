package com.digitalawayday.process;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.Arrays;

import org.junit.Test;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;

public class DefaultActivityScheduleFileWriterTest {

    private static final Activity TEN_MIN = new Activity(10, "TEN_MIN");

    private static final Activity FIFTEEN_MIN = new Activity(15, "FIFTEEN_MIN");

    private static final Activity FORTYFIVE_MIN = new Activity(45, "FORTYFIVE_MIN");

    private static final Activity ONE_HOUR_15MIN = new Activity(75, "ONE_HOUR_15MIN");

    private static final Activity ONE_HOUR_35MIN = new Activity(95, "ONE_HOUR_35MIN");

    private static final Activity TWO_HOURS = new Activity(120, "TWO_HOURS");

    private static final Activity TWO_HOURS_25MIN = new Activity(145, "TWO_HOURS_25MIN");

    private static final Activity THREE_HOURS = new Activity(180, "THREE_HOURS");

    private static final Activity FOUR_HOURS = new Activity(240, "FOUR_HOURS");

    private static final String EXPECTED_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/writer_output_expected.txt";

    private static final String OUTPUT_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/writer_output.txt";

    private DefaultActivityScheduleFileWriter writer = new DefaultActivityScheduleFileWriter();

    @Test
    public void testTimeAfterNine() {
        assertThat(writer.getTimeAfterNine(195)).isEqualTo("12:15 pm");
    }

    @Test
    public void testTimeBeforeFive() {
        assertThat(writer.getTimeBeforeFive(76)).isEqualTo("03:44 pm");
    }

    @Test
    public void testWriter() {

        TeamDayPlan planA = new TeamDayPlan();
        planA.addActivityBeforeLunch(ONE_HOUR_15MIN, ONE_HOUR_15MIN, FIFTEEN_MIN);
        planA.addActivityAfterLunch(TWO_HOURS_25MIN, ONE_HOUR_35MIN);
        planA.setPresentationTimeAfterLunch(15);

        TeamDayPlan planB = new TeamDayPlan();
        planB.addActivityBeforeLunch(ONE_HOUR_15MIN, FORTYFIVE_MIN);
        planB.addActivityAfterLunch(TWO_HOURS_25MIN, ONE_HOUR_35MIN);
        planB.setPresentationTimeAfterLunch(15);
        writer.write(OUTPUT_FILE, Arrays.asList(planA, planB));
        assertThat(new File(OUTPUT_FILE)).hasSameContentAs(new File(EXPECTED_FILE));
    }

}
