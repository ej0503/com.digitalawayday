package com.digitalawayday.process;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.digitalawayday.model.Activity;

public class ActivityDurationDescComparatorTest {
    private static final Activity SHORTEST = new Activity(5, "SHORTEST");

    private static final Activity SHORT = new Activity(5, "SHORT");

    private static final Activity MIDDLE = new Activity(10, "MIDDLE");

    private static final Activity LONG = new Activity(20, "LONG");

    private static final Activity LONGER = new Activity(40, "LONGER");

    @Test
    public void testCompare() {
        List<Activity> activities = Arrays.asList(new Activity[] { SHORT, LONGER, MIDDLE, SHORTEST, LONG });

        Collections.sort(activities, ActivityDurationDescComparator.INSTANCE);

        assertThat(activities).as("Sorted Activity list in descending order.").containsExactly(LONGER, LONG, MIDDLE,
                SHORT, SHORTEST);
    }

}
