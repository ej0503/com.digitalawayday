package com.digitalawayday.process;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.List;

import org.junit.Test;

import com.digitalawayday.model.Activity;
import com.digitalawayday.process.strategy.DefaultActivitySchedulerStrategy;

public class DefaultActivitySchedulerTest {

    private static final String INPUT_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/activities.txt";

    private static final String OUTPUT_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/scheduler_output.txt";

    private static final String BACKTRACKING_OUTPUT_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/scheduler_backtracking_output.txt";

    private static final String EXPECTED_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/scheduler_output_expected.txt";

    private static final String BACKTRACKING_EXPECTED_FILE = "/home/edward/git_ej0503/com.digitalawayday/test-data/scheduler_backtracking_output_expected.txt";

    private static final Activity[] INPUT_ACTIVITIES = { new Activity(60, "Duck Herding"), new Activity(45, "Archery"),
            new Activity(40, "Learning Magic Tricks"), new Activity(60, "Laser Clay Shooting"),
            new Activity(30, "Human Table Football"), new Activity(30, "Buggy Driving"),
            new Activity(15, "Salsa & Pickles"), new Activity(45, "2-wheeled Segways"),
            new Activity(60, "Viking Axe Throwing"), new Activity(30, "Giant Puzzle Dinosaurs"),
            new Activity(60, "Giant Digital Graffiti"), new Activity(60, "Cricket 2020"),
            new Activity(15, "Wine Tasting"), new Activity(30, "Arduino Bonanza"),
            new Activity(60, "Digital Tresure Hunt"), new Activity(45, "Enigma Challenge"),
            new Activity(60, "Monti Carlo or Bust"), new Activity(30, "New Zealand Haka"),
            new Activity(15, "Time Tracker"), new Activity(45, "Indiano Drizzle") };

    private DefaultActivityScheduler scheduler = new DefaultActivityScheduler(new DefaultActivityParser(),
            new DefaultActivitySchedulerStrategy(), new DefaultActivityScheduleFileWriter());

    @Test
    public void testProcess() {
        assertThat(scheduler.process(INPUT_FILE, OUTPUT_FILE))
                .as("Expected a solution to be found for processing " + OUTPUT_FILE).isTrue();
        assertThat(new File(OUTPUT_FILE)).hasSameContentAs(new File(EXPECTED_FILE));
    }

    @Test
    public void testProcessWithBacktracking() {
        assertThat(ActivityFactory.scheduler().process(INPUT_FILE, BACKTRACKING_OUTPUT_FILE))
                .as("Expected a solution to be found for processing " + BACKTRACKING_OUTPUT_FILE).isTrue();
        assertThat(new File(BACKTRACKING_OUTPUT_FILE)).hasSameContentAs(new File(BACKTRACKING_EXPECTED_FILE));
    }

    @Test
    public void testConvertFileToList() {
        List<Activity> list = scheduler.convertFileToActivities(INPUT_FILE);
        assertThat(list).as("Input Activities from activities.txt").containsExactly(INPUT_ACTIVITIES);
    }

}
