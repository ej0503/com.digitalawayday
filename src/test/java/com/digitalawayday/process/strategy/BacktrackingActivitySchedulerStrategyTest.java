package com.digitalawayday.process.strategy;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;

public class BacktrackingActivitySchedulerStrategyTest {

    private static final Activity TEN_MIN = new Activity(10, "TEN_MIN");

    private static final Activity FIFTEEN_MIN = new Activity(15, "FIFTEEN_MIN");

    private static final Activity FORTYFIVE_MIN = new Activity(45, "FORTYFIVE_MIN");

    private static final Activity ONE_HOUR_15MIN = new Activity(75, "ONE_HOUR_15MIN");

    private static final Activity ONE_HOUR_35MIN = new Activity(95, "ONE_HOUR_35MIN");

    private static final Activity TWO_HOURS = new Activity(120, "TWO_HOURS");

    private static final Activity TWO_HOURS_25MIN = new Activity(145, "TWO_HOURS_25MIN");

    private static final Activity THREE_HOURS = new Activity(180, "THREE_HOURS");

    private static final Activity FOUR_HOURS = new Activity(240, "FOUR_HOURS");

    private BacktrackingActivitySchedulerStrategy strategy = new BacktrackingActivitySchedulerStrategy();

    @Test
    public void testDurationLessThanMinimumDayDurationFails() {
        List<Activity> activities = Arrays.asList(new Activity[] { THREE_HOURS, TWO_HOURS });
        assertThat(strategy.resolvePlan(activities)).isEmpty();
    }

    @Test
    public void testRemainingDurationGreaterThanMarginFails() {
        List<Activity> activities = Arrays
                .asList(new Activity[] { THREE_HOURS, THREE_HOURS, THREE_HOURS, THREE_HOURS, THREE_HOURS });

        assertThat(strategy.resolvePlan(activities)).isEmpty();
    }

    @Test
    public void testStraightForwardExample() {
        List<Activity> activities = Arrays.asList(new Activity[] { THREE_HOURS, FORTYFIVE_MIN, THREE_HOURS,
                FORTYFIVE_MIN, THREE_HOURS, TEN_MIN, THREE_HOURS, FIFTEEN_MIN });

        TeamDayPlan planA = new TeamDayPlan();
        planA.addActivityBeforeLunch(THREE_HOURS);
        planA.addActivityAfterLunch(THREE_HOURS, FORTYFIVE_MIN, FIFTEEN_MIN);

        TeamDayPlan planB = new TeamDayPlan();
        planB.addActivityBeforeLunch(THREE_HOURS);
        planB.addActivityAfterLunch(THREE_HOURS, FORTYFIVE_MIN, TEN_MIN);

        SoftAssertions softly = new SoftAssertions();
        Optional<List<TeamDayPlan>> plan = strategy.resolvePlan(activities);
        softly.assertThat(plan).isPresent();

        softly.assertThat(plan.get()).containsExactly(planA, planB);

        softly.assertAll();
    }

    @Test
    public void testMorningsDontFit() {
        List<Activity> activities = Arrays.asList(new Activity[] { FOUR_HOURS, FOUR_HOURS, TWO_HOURS, TWO_HOURS,
                FORTYFIVE_MIN, FORTYFIVE_MIN, FIFTEEN_MIN, TEN_MIN });

        TeamDayPlan planA = new TeamDayPlan();
        planA.addActivityBeforeLunch(TWO_HOURS, FORTYFIVE_MIN, FIFTEEN_MIN);
        planA.addActivityAfterLunch(FOUR_HOURS);

        TeamDayPlan planB = new TeamDayPlan();
        planB.addActivityBeforeLunch(TWO_HOURS, FORTYFIVE_MIN, TEN_MIN);
        planB.addActivityAfterLunch(FOUR_HOURS);

        SoftAssertions softly = new SoftAssertions();
        Optional<List<TeamDayPlan>> plan = strategy.resolvePlan(activities);
        softly.assertThat(plan).isPresent();

        softly.assertThat(plan.get()).containsExactly(planA, planB);

        softly.assertAll();
    }

    @Test
    public void testHasToFillAfternoonFirst() {
        List<Activity> activities = Arrays.asList(new Activity[] { TWO_HOURS_25MIN, TWO_HOURS_25MIN, ONE_HOUR_35MIN,
                ONE_HOUR_35MIN, ONE_HOUR_15MIN, ONE_HOUR_15MIN, FORTYFIVE_MIN, ONE_HOUR_15MIN });

        TeamDayPlan planA = new TeamDayPlan();
        planA.addActivityBeforeLunch(TWO_HOURS_25MIN);
        planA.addActivityAfterLunch(TWO_HOURS_25MIN, ONE_HOUR_35MIN);

        TeamDayPlan planB = new TeamDayPlan();
        planB.addActivityBeforeLunch(ONE_HOUR_15MIN, ONE_HOUR_15MIN);
        planB.addActivityAfterLunch(ONE_HOUR_35MIN, ONE_HOUR_15MIN, FORTYFIVE_MIN);

        SoftAssertions softly = new SoftAssertions();
        Optional<List<TeamDayPlan>> plan = strategy.resolvePlan(activities);

        softly.assertThat(plan).isPresent();
        softly.assertThat(plan.get()).containsExactly(planA, planB);
        softly.assertAll();
    }

    @Test
    public void testNeedToFillMorningFirst() {
        List<Activity> activities = Arrays.asList(new Activity[] { THREE_HOURS, TWO_HOURS_25MIN, ONE_HOUR_35MIN });

        TeamDayPlan planA = new TeamDayPlan();
        planA.addActivityBeforeLunch(THREE_HOURS);
        planA.addActivityAfterLunch(TWO_HOURS_25MIN, ONE_HOUR_35MIN);

        SoftAssertions softly = new SoftAssertions();
        Optional<List<TeamDayPlan>> plan = strategy.resolvePlan(activities);

        softly.assertThat(plan).isPresent();
        softly.assertThat(plan.get()).containsExactly(planA);
        softly.assertAll();
    }
}
