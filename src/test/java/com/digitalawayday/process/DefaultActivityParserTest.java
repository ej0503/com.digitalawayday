package com.digitalawayday.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.Test;

import com.digitalawayday.model.Activity;

public class DefaultActivityParserTest {

    private DefaultActivityParser parser = new DefaultActivityParser();

    @Test
    public void testParse() {
        assertThat(parser.parse("Duck Herding 60min")).as("'Duck Herding 60min' attempted to be parsed.")
                .isEqualTo(new Activity(60, "Duck Herding"));
    }

    @Test
    public void testParseSprint() {
        assertThat(parser.parse("Time Tracker sprint")).as("'Time Tracker sprint' attempted to be parsed.")
                .isEqualTo(new Activity(15, "Time Tracker"));
    }

    @Test
    public void testParseBNotEndInMin() {
        Throwable throwable = catchThrowable(() -> {
            parser.parse("Duck Herding 60mint");
        });
        assertThat(throwable).as("\"Attempting to parse 'Duck Herding 60mint'.")
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Duration in wrong format to parse as Activity: 60mint");
    }

    @Test
    public void testParseBadInteger() {
        Throwable throwable = catchThrowable(() -> {
            parser.parse("Duck Herding Sixtymin");
        });
        assertThat(throwable).as("Attempting to parse 'Duck Herding Sixtymin'.")
                .isInstanceOf(NumberFormatException.class).hasMessage("For input string: \"Sixty\"");
    }
}
