package com.digitalawayday.model;

import java.util.Objects;

/**
 * <p>
 * Immutable data class to hold the {@code duration} and {@code label} for an
 * {@link Activity}.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public class Activity {

    private final int duration;

    private final String label;

    public Activity(final int duration, final String label) {
        this.duration = duration;
        this.label = label;
    }

    public int getDuration() {
        return duration;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(duration, label);
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Activity))
            return false;
        Activity other = (Activity) obj;
        return Objects.equals(duration, other.duration) && Objects.equals(label, other.label);
    }

    @Override
    public String toString() {
        return String.format("Activity[%dmin, %s]", duration, label);
    }

}
