package com.digitalawayday.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Data class for the activities held for each team, stored either before or
 * after lunch.
 * 
 * @author edward.joyce
 *
 */
public class TeamDayPlan {

    public static final int DAY_START = 9;

    public static final int LUNCH_START = 12;

    public static final int LUNCH_END = 13;

    public static final int DAY_END_MIN = 16;

    public static final int DAY_END_MAX = 17;

    public static final int DURATION_BEFORE_LUNCH = 60 * (LUNCH_START - DAY_START);

    public static final int DURATION_AFTER_LUNCH_MIN = 60 * (DAY_END_MIN - LUNCH_END);

    public static final int DURATION_AFTER_LUNCH_MAX = 60 * (DAY_END_MAX - LUNCH_END);

    public static final int DURATION_TOTAL_DAY_MIN = DURATION_BEFORE_LUNCH + DURATION_AFTER_LUNCH_MIN;

    public static final int DURATION_TOTAL_DAY_MARGIN = DURATION_AFTER_LUNCH_MAX - DURATION_AFTER_LUNCH_MIN;

    private final List<Activity> beforeLunch = new ArrayList<>();

    private final List<Activity> afterLunch = new ArrayList<>();

    private int remainingDurationBeforeLunch = DURATION_BEFORE_LUNCH;

    private int remainingDurationAfterLunch = DURATION_AFTER_LUNCH_MAX;

    private int presentationTimeAfterLunch = 0;

    public boolean isActivityShortEnoughBeforeLunch(Activity activity) {
        return remainingDurationBeforeLunch - activity.getDuration() >= 0;
    }

    public void addActivityBeforeLunch(Activity... activities) {
        Arrays.asList(activities).stream().forEach(act -> addActivityBeforeLunch(act));
    }

    public void addActivityBeforeLunch(Activity activity) {
        beforeLunch.add(activity);
        remainingDurationBeforeLunch -= activity.getDuration();
        if (remainingDurationBeforeLunch < 0) {
            throw new IllegalStateException(String.format("Remaining time before lunch <0 after adding %s.", activity));
        }
    }

    public boolean isActivityShortEnoughAfterLunch(Activity activity) {
        return remainingDurationAfterLunch - activity.getDuration() >= 0;
    }

    public void addActivityAfterLunch(Activity... activities) {
        Arrays.asList(activities).stream().forEach(act -> addActivityAfterLunch(act));
    }

    public void addActivityAfterLunch(Activity activity) {
        afterLunch.add(activity);
        remainingDurationAfterLunch -= activity.getDuration();
        if (remainingDurationAfterLunch < 0) {
            throw new IllegalStateException(String.format("Remaining time after lunch <0 after adding %s.", activity));
        }
    }

    public List<Activity> getBeforeLunch() {
        return Collections.unmodifiableList(beforeLunch);
    }

    public List<Activity> getAfterLunch() {
        return Collections.unmodifiableList(afterLunch);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(beforeLunch, afterLunch);
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof TeamDayPlan))
            return false;
        TeamDayPlan other = (TeamDayPlan) obj;
        return Objects.equals(beforeLunch, other.beforeLunch) && Objects.equals(afterLunch, other.afterLunch);
    }

    @Override
    public String toString() {
        return String.format("(Before:%s, After:%s)", beforeLunch, afterLunch);
    }

    public int getRemainingDurationBeforeLunch() {
        return remainingDurationBeforeLunch;
    }

    public int getRemainingDurationAfterLunch() {
        return remainingDurationAfterLunch;
    }

    public void setPresentationTimeAfterLunch(int presentationTimeAfterLunch) {
        this.presentationTimeAfterLunch = presentationTimeAfterLunch;
    }

    public int getPresentationTimeAfterLunch() {
        return presentationTimeAfterLunch;
    }
}
