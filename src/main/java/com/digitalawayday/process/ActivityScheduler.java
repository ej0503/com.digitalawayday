package com.digitalawayday.process;

/**
 * 
 * <p>
 * A scheduler for a list of activities, reading and writing to files.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public interface ActivityScheduler {

    /**
     * Parses activities from an input file, creates a schedule, and writes to an
     * output file.
     * 
     * @param inputFileLocation
     *            The location to read the activities from.
     * @param outputFileLocation
     *            The location to write the schedule to.
     * @return {@code true} if a solution was found, {@code false} if not.
     */
    boolean process(String inputFileLocation, String outputFileLocation);
}
