package com.digitalawayday.process;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pmw.tinylog.Logger;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;
import com.digitalawayday.process.strategy.ActivitySchedulerStrategy;

/**
 * <p>
 * Default implementation of {@link ActivityScheduler}.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public class DefaultActivityScheduler implements ActivityScheduler {

    private final ActivityParser parser;

    private final ActivitySchedulerStrategy strategy;

    private final ActivityWriter writer;

    public DefaultActivityScheduler(final ActivityParser parser, final ActivitySchedulerStrategy strategy,
            final ActivityWriter writer) {
        this.parser = parser;
        this.strategy = strategy;
        this.writer = writer;
    }

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public boolean process(final String inputFileLocation, final String outputFileLocation) {
        if (!new File(inputFileLocation).isFile()) {
            Logger.error("{}  isn't a readable file.", inputFileLocation);
            return false;
        }
        List<Activity> list = convertFileToActivities(inputFileLocation);

        Logger.info("Creating schedule from input file: {} and writing to: {}", inputFileLocation, outputFileLocation);
        Optional<List<TeamDayPlan>> plans = strategy.resolvePlan(list);

        if (!plans.isPresent()) {
            return false;
        }
        writer.write(outputFileLocation, plans.get());
        return true;
    }

    List<Activity> convertFileToActivities(final String inputFileLocation) {
        List<Activity> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(inputFileLocation))) {

            list = stream.map(line -> parser.parse(line)).collect(Collectors.toList());

        } catch (IOException e) {
            Logger.error(e);
        }
        return list;
    }
}
