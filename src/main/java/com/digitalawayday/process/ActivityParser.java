package com.digitalawayday.process;

import com.digitalawayday.model.Activity;

/**
 * <p>
 * Parses lines from activity input file into {@link Activity} instances.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public interface ActivityParser {

    /**
     * Creates a new {@link Activity} from line.
     * 
     * @param line
     *            A {@link String} from the activity input file.
     * @return An {@link Activity} with a corresponding label and duration.
     * @throws IllegalArgumentException
     *             if the line does not end with a duration in the form of "sprint"
     *             or "Xmin".
     * @throws NumberFormatException
     *             if X is not parseable by {@code Integer.parseInt(X)}.
     */
    Activity parse(String line);

}
