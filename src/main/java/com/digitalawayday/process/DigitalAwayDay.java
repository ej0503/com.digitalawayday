package com.digitalawayday.process;

import org.pmw.tinylog.Logger;

/**
 * 
 * <p>
 * Main entry point for the DigitalAwayDay activity scheduler.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public class DigitalAwayDay {

    public static void main(String[] args) {
        if (args == null || args.length != 2) {
            Logger.error("Two arguments, the path of an input and output file, are required.");
            return;
        }
        ActivityFactory.scheduler().process(args[0], args[1]);
    }
}
