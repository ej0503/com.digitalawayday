package com.digitalawayday.process;

import com.digitalawayday.process.strategy.ActivitySchedulerStrategy;
import com.digitalawayday.process.strategy.BacktrackingActivitySchedulerStrategy;

/**
 * 
 * <p>
 * Singleton static Factory used to determine the implementation used for each
 * interface.
 * </p>
 * <p>
 * Replaceable with Spring configuration in a larger application.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public enum ActivityFactory {
    INSTANCE;

    static ActivityScheduler scheduler() {
        return new DefaultActivityScheduler(parser(), strategy(), writer());
    }

    static ActivitySchedulerStrategy strategy() {
        return new BacktrackingActivitySchedulerStrategy();
    }

    static ActivityParser parser() {
        return new DefaultActivityParser();
    }

    static ActivityWriter writer() {
        return new DefaultActivityScheduleFileWriter();
    }
}
