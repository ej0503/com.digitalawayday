package com.digitalawayday.process;

import java.util.List;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;

/**
 * 
 * <p>
 * Interface for writing the {@link Activity} schedule to file, in the following
 * format:
 * <ul>
 * <li>Each {@link TeamDayPlan} will be output with an hour lunch break, and its
 * activities placed outward from it with no gaps in between. This may leave a
 * gap either side.</li>
 * <li>The Staff Motivation Presentation will take place at the earliest time
 * available to all teams.</li>
 * </p>
 * 
 * @author edward.joyce
 *
 */
public interface ActivityWriter {

    /**
     * <p>
     * Writes the plans to file in the required format.
     * </p>
     * 
     * @param filePath
     *            The file to write to.
     * @param plans
     *            The plans for each team on the activity day.
     */
    void write(String filePath, List<TeamDayPlan> plans);
}
