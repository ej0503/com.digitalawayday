package com.digitalawayday.process;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;

/**
 * Default implementation of {@link ActivityWriter}.
 * 
 * @author edward.joyce
 *
 */
public class DefaultActivityScheduleFileWriter implements ActivityWriter {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");

    private static final LocalDateTime FIVE_PM = LocalDateTime.of(1970, 1, 1, 17, 0);

    private static final LocalDateTime NINE_AM = LocalDateTime.of(1970, 1, 1, 9, 0);

    private static final Activity LUNCH = new Activity(60, "Lunch Break");

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public void write(String filePath, List<TeamDayPlan> plans) {
        Path path = Paths.get(filePath);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {

            int teamNo = 1;
            for (TeamDayPlan plan : plans) {
                writer.write(String.format("Team %d:\n", teamNo++));
                writePlan(writer, plan);
                writer.write('\n');
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writePlan(BufferedWriter writer, TeamDayPlan plan) throws IOException {
        int minAfterNine = plan.getRemainingDurationBeforeLunch();

        for (Activity activity : plan.getBeforeLunch()) {
            writeActivity(writer, minAfterNine, activity);
            minAfterNine += activity.getDuration();
        }
        writeActivity(writer, minAfterNine, LUNCH);
        minAfterNine += LUNCH.getDuration();

        for (Activity activity : plan.getAfterLunch()) {
            writeActivity(writer, minAfterNine, activity);
            minAfterNine += activity.getDuration();
        }
        writePresentation(writer, plan.getPresentationTimeAfterLunch());
    }

    private void writeActivity(BufferedWriter writer, int minAfterNine, Activity activity) throws IOException {
        writer.write(String.format("%s : %s %s\n", getTimeAfterNine(minAfterNine), activity.getLabel(),
                durationToString(activity.getDuration())));
    }

    private String durationToString(int duration) {
        return duration == 15 ? "sprint" : duration + "min";
    }

    private void writePresentation(BufferedWriter writer, int minAfterLunch) throws IOException {
        writer.write(String.format("%s : Staff Motivation Presentation\n", getTimeBeforeFive(minAfterLunch)));
    }

    String getTimeAfterNine(int minAfterNine) {
        return NINE_AM.plusMinutes(minAfterNine).format(formatter);
    }

    String getTimeBeforeFive(int minAfterLunch) {
        return FIVE_PM.minusMinutes(minAfterLunch).format(formatter);
    }
}
