package com.digitalawayday.process;

import com.digitalawayday.model.Activity;

/**
 * Default implementation of {@link ActivityParser}.
 * 
 * @author edward.joyce
 *
 */
public class DefaultActivityParser implements ActivityParser {

    private static final String SPRINT = "sprint";

    private static final String MIN = "min";

    private static final int MIN_LENGTH = 3;

    private static final char DELIMITER = ' ';

    private static final int SPRINT_DURATION = 15;

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public Activity parse(String string) {
        int index = string.lastIndexOf(DELIMITER);
        String duration = string.substring(index + 1);
        String label = string.substring(0, index);
        Integer dur = parseDuration(duration);
        return new Activity(dur, label);
    }

    private int parseDuration(String duration) {
        if (SPRINT.equals(duration)) {
            return SPRINT_DURATION;
        }
        assertDurationEndsInMin(duration);
        String durationMin = parseDurationMin(duration);
        return Integer.parseInt(durationMin);
    }

    private String parseDurationMin(String duration) {
        return duration.substring(0, duration.length() - MIN_LENGTH);
    }

    private void assertDurationEndsInMin(String duration) {
        throwException(!duration.endsWith(MIN), duration);
    }

    private void throwException(boolean doThrow, String duration) {
        if (doThrow) {
            throw new IllegalArgumentException(
                    String.format("Duration in wrong format to parse as Activity: %s", duration));
        }
    }

}
