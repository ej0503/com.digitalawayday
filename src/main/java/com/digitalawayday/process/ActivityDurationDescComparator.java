package com.digitalawayday.process;

import java.util.Comparator;

import com.digitalawayday.model.Activity;

/**
 * <p>
 * Sort {@link Activity} instances in descending order of {@code duration}.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public enum ActivityDurationDescComparator implements Comparator<Activity> {
    INSTANCE;

    @Override
    public int compare(Activity arg0, Activity arg1) {
        return arg1.getDuration() - arg0.getDuration();
    }

}
