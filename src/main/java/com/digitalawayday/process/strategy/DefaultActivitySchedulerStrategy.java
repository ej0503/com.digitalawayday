package com.digitalawayday.process.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.pmw.tinylog.Logger;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;
import com.digitalawayday.process.ActivityDurationDescComparator;

/**
 * <p>
 * Implementation of {@link ActivitySchedulerStrategy}.
 * </p>
 * 
 * <p>
 * The algorithm will follow the following steps:
 * <ul>
 * <li>The total duration will be calculated by summing over the activities, and
 * used to set the number of teams by dividing by the minimum activity duration
 * per day, 6 hours.
 * <li>If the total duration is less than the minimum activity duration, the
 * strategy will return immediately.
 * <li>If the remaining activity duration after filling full minimum days is
 * greater than the allowed margin of one hour per day (difference between a
 * minimum and maximum day) the strategy will return immediately.
 * <li>The activities will be sorted in descending order of duration.
 * <li>Empty plans for each time will then be created.
 * <li>Each activity is attempted to be slotted into the next plan, afternoon
 * first.
 * <li>If the activities are unable to be added to that plan, the next is
 * attempted.
 * <li>If all the activities fit, the schedule has been created successfully. If
 * not, the strategy will return, but a failure case has not yet been found that
 * has a total duration within margin.
 * </ul>
 * </p>
 * 
 * @author edward.joyce
 *
 */
public class DefaultActivitySchedulerStrategy implements ActivitySchedulerStrategy {

    @Override
    public Optional<List<TeamDayPlan>> resolvePlan(final List<Activity> activities) {
        Collections.sort(activities, ActivityDurationDescComparator.INSTANCE);
        int totalDuration = activities.stream().mapToInt(act -> act.getDuration()).sum();

        if (totalDuration < TeamDayPlan.DURATION_TOTAL_DAY_MIN) {
            Logger.info("The total duration of activities is too low to fill one day adequately.");
            return Optional.empty();
        }
        int numberOfDays = totalDuration / TeamDayPlan.DURATION_TOTAL_DAY_MIN;
        int availableMargin = numberOfDays * TeamDayPlan.DURATION_TOTAL_DAY_MARGIN;
        int totalDurationRemaining = totalDuration % TeamDayPlan.DURATION_TOTAL_DAY_MIN;

        if (availableMargin < totalDurationRemaining) {
            Logger.info(
                    "The total duration of activities does not allow teams to be formed with full days adequately.");
            return Optional.empty();
        }
        return populateTeamDayPlans(activities, numberOfDays);
    }

    Optional<List<TeamDayPlan>> populateTeamDayPlans(final List<Activity> activities, int numberOfDays) {
        List<TeamDayPlan> plans = createPlans(numberOfDays);
        int p = 0;
        int a = 0;
        int failures = 0;

        while (a < activities.size()) {
            Activity act = activities.get(a);
            TeamDayPlan plan = plans.get(p);

            if (plan.isActivityShortEnoughAfterLunch(act)) {
                plan.addActivityAfterLunch(act);
                a++;
                failures = 0;

            } else if (plan.isActivityShortEnoughBeforeLunch(act)) {
                plan.addActivityBeforeLunch(act);
                a++;
                failures = 0;

            } else if (++failures >= numberOfDays) {
                Logger.info("The composition of activities is unable to be sorted into a schedule.");
                return Optional.empty();
            }
            p = nextPlanIndex(numberOfDays, p);
        }
        assignPresentationTime(plans);
        return Optional.of(plans);
    }

    void assignPresentationTime(List<TeamDayPlan> plans) {
        int lowest = TeamDayPlan.DURATION_AFTER_LUNCH_MAX;
        for (TeamDayPlan plan : plans) {
            if (lowest > plan.getRemainingDurationAfterLunch()) {
                lowest = plan.getRemainingDurationAfterLunch();
            }
        }
        for (TeamDayPlan plan : plans) {
            plan.setPresentationTimeAfterLunch(lowest);
        }
    }

    /**
     * <p>
     * Returns integers 0 to {@code d < numberOfDays} in an infinite cycle.
     * </p>
     * 
     * @param numberOfDays
     * @param d
     * @return The next value of {@code d}.
     */
    private int nextPlanIndex(int numberOfDays, int d) {
        return (d == numberOfDays - 1) ? d = 0 : d + 1;
    }

    protected List<TeamDayPlan> createPlans(int numberOfDays) {
        List<TeamDayPlan> plans = new ArrayList<>();
        for (int d = 0; d < numberOfDays; d++) {
            plans.add(new TeamDayPlan());
        }
        return plans;
    }
}
