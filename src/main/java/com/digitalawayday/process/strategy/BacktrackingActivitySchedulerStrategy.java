package com.digitalawayday.process.strategy;

import java.util.List;
import java.util.Optional;

import org.pmw.tinylog.Logger;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;

/**
 * <p>
 * Implementation of {@link ActivitySchedulerStrategy} that recursively
 * backtracks. This makes it less greedy than
 * {@link DefaultActivitySchedulerStrategy}, and so can solve solutions the
 * latter cannot.
 * </p>
 * 
 * <p>
 * The algorithm takes the same initial steps as
 * {@link DefaultActivitySchedulerStrategy}, and then differs once it starts to
 * populate the plans.
 * <ul>
 * <li>Each node takes a list of activities, the index of the activity to use,
 * the index of the plan to add to and whether to add to the afternoon or
 * morning slot.
 * <li>If the activity index reaches the end of the list, all activities have
 * been added successfully and the plan is returned.
 * <li>Otherwise the activity is attempted to be added into the plan in either
 * the specified afternoon or morning. If the desired action cannot be taken, no
 * branching happens and {@code Optional.empty()} is immediately returned.
 * <li>If successful, the method is recursively called, for the afternoon or
 * morning slot for each plan with the same incremented activity index.
 * <li>If any of the branches are successful (Optional.isPresent() is returned)
 * the return value is immediately returned, ending any branching.
 * <li>If none of the branches were successful, Optional.empty() is returned.
 * </ul>
 * </p>
 * 
 * <p>
 * In worst case, the time complexity is {@code O((2P)^A)}, as we will have
 * fully searched a tree with a depth equal to the number of Activities,
 * branching 2 * number of Plans each time. However, in most cases there will be
 * many possible solutions so a full search isn't required and we return and
 * stop branching as soon as a solution is found. Ordering the activities in
 * descending order and preferring adding first to the afternoon (as it is an
 * hour longer so more flexibility) should generally lead us towards solutions
 * faster.
 * </p>
 * 
 * 
 * @author edward.joyce
 *
 */
public class BacktrackingActivitySchedulerStrategy extends DefaultActivitySchedulerStrategy {

    @Override
    Optional<List<TeamDayPlan>> populateTeamDayPlans(final List<Activity> activities, int numberOfDays) {

        Optional<List<TeamDayPlan>> plans = recursivePopulation(activities, createPlans(numberOfDays), 0, 0, true);

        if (!plans.isPresent()) {
            plans = recursivePopulation(activities, createPlans(numberOfDays), 0, 0, false);
            if (!plans.isPresent()) {
                return Optional.empty();
            }
        }

        assignPresentationTime(plans.get());
        return plans;
    }

    Optional<List<TeamDayPlan>>

            recursivePopulation(final List<Activity> activities, List<TeamDayPlan> plans, int a, int p,
                    boolean isForAfternoon) {

        if (a >= activities.size()) {
            Logger.info("Node[{}{}{}] Successfully found a solution.", a, p, isForAfternoon);
            return Optional.of(plans);
        }
        Activity act = activities.get(a);
        TeamDayPlan plan = plans.get(p);

        if (isForAfternoon && plan.isActivityShortEnoughAfterLunch(act)) {
            Logger.debug("Node[{}{}{}] Adding {} to afternoon.", a, p, isForAfternoon, act);
            plan.addActivityAfterLunch(act);

        } else if (!isForAfternoon && plan.isActivityShortEnoughBeforeLunch(act)) {
            Logger.debug("Node[{}{}{}] Adding {} to morning.", a, p, isForAfternoon, act);
            plan.addActivityBeforeLunch(act);

        } else {
            // desired action cannot be taken
            Logger.debug("Desired action cannot be taken, failed solution found, return.");
            return Optional.empty();
        }
        // branching, applying activity to each
        a++;
        for (int nextP = 0; nextP < plans.size(); nextP++) {
            Optional<List<TeamDayPlan>> afternoon = recursivePopulation(activities, plans, a, nextP, true);

            if (afternoon.isPresent()) {
                Logger.debug("Node[{}{}{}] Solution returned from branch.", a, p, isForAfternoon);
                return afternoon;
            }
            Optional<List<TeamDayPlan>> morning = recursivePopulation(activities, plans, a, nextP, false);

            if (morning.isPresent()) {
                Logger.debug("Node[{}{}{}] Solution returned from branch.", a, p, isForAfternoon);
                return morning;
            }
        }

        Logger.debug("Node[{}{}{}] No branches were successful, return.", a, p, isForAfternoon);
        return Optional.empty();
    }
}
