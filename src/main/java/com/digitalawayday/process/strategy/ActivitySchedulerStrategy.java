package com.digitalawayday.process.strategy;

import java.util.List;
import java.util.Optional;

import com.digitalawayday.model.Activity;
import com.digitalawayday.model.TeamDayPlan;
import com.digitalawayday.process.ActivityScheduler;

/**
 * <p>
 * Strategy for the {@link ActivityScheduler} to create an {@link ActivityPlan}.
 * </p>
 * 
 * @author edward.joyce
 *
 */
public interface ActivitySchedulerStrategy {

    /**
     * Sorts activities into an {@link ActivityPlan}, according to the following
     * rules:
     * <ul>
     * <li>The activities are to be divided across as many teams as necessary.</li>
     * <li>The activities start from 9am until an hour lunch is served at 12
     * noon.</li>
     * <li>The activities resume from 1pm and must finish between 4:00 and
     * 5:00pm.</li>
     * <li>The activities must have no gap.</li>
     * </ul>
     * 
     * @param activities
     * @return An ActivityPlan that matches the above criteria. If it cannot be
     *         matched, {@link
     */
    Optional<List<TeamDayPlan>> resolvePlan(List<Activity> activities);
}
