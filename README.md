# com.digitalawayday


Setup
--------------
1. Extract from the zip file, and open up the commandline inside the folder. There should be a *digitalawayday-edwardjoyce_backtracking.jar* and *activities.txt*.
2. Run the following command:
 
    java -jar digitalawayday-edwardjoyce_backtracking.jar activities.txt output.txt
    
This will generate an output.txt file. Using anything other than two arguments, or an unusable input file, will result in an error.


Class Design Justifications and Explanations
--------------
**1. DigitalAwayDay**

- Running via commandline seemed the simplest way to interactively test the program. This is the main class specified in the manifest.

**2. DefaultActivityScheduler**

- The input fileLocation String is checked to provide helpful error logging in the case of human IO error.

**3. DefaultActivityParser**

- Each line is split by the last space into a label and a duration.
- The duration is checked to end in "min" (and its prefix resolve to an integer) or equals sprint.
  An IllegalArgumentException is thrown if either is not met.

**4. DefaultActivitySchedulerStrategy (for actual Strategy used see 9. below.)**
The algorithm will follow the following steps (copy and pasted from Javadoc):

- The total duration will be calculated by summing over the activities, and
used to set the number of teams by dividing by the minimum activity duration
per day, 6 hours.
- If the total duration is less than the minimum activity duration, the
strategy will return immediately Optional.empty().
- If the remaining activity duration after filling full minimum days is
greater than the allowed margin of one hour per day (difference between a
minimum and maximum day) the strategy will return immediately.
- The activities will be sorted in descending order of duration.
- Empty day plans for each team will then be created.
- Each activity is attempted to be slotted into the next plan, afternoon
first.
- If the activities are unable to be added to that plan, the next is
attempted.
- If all the activities fit, the schedule has been created successfully. If
not, the strategy will return, but a failure case has not yet been found that
has a total duration within margin.

**5. TeamDayPlan**

- Equals and Hashcode do not take into account the mutable fields
*remainingDurationBeforeLunch*, *remainingDurationAfterLunch* and *presentationTimeAfterLunch*
as they are calculated from the activity lists. *IllegalStateExceptions* are thrown if the above *remainingDuration* fields drop below zero.
 
**6. ActivityDurationDescComparator**

- Comparator added to sort Activities in descending order of time, as a preliminary step.
 
**7. DefaultActivityScheduleFileWriter**

Copied from javadoc of interface:

- Each *TeamDayPlan* will be output with an hour lunch break, and its activities placed outward from it with no gaps in between. This may leave a
gap either side.
- The Staff Motivation Presentation will take place at the earliest time
available to all teams.

**8. Project setup**

- Git has been used for version control.
- Maven for building, using apache-shade-plugin.
- SpringToolSuite (Eclipse) used as IDE.
- JUnit with AssertJ for test suite, and EqualsVerifier for testing equals() and hashcode() on data classes.
- tinylog for a lightweight static logging framework (in a larger project this could be replaced with logback.)
- In a larger project Spring could be added for dependency injection.

**9. BacktrackingActivitySchedulerStrategy**

Copied from the javadoc.

-Implementation of *ActivitySchedulerStrategy* that recursively
backtracks. This makes it less greedy than
*DefaultActivitySchedulerStrategy*, and so can solve solutions the
latter cannot.

- The algorithm takes the same initial steps as
*DefaultActivitySchedulerStrategy*, and then differs once it starts to
populate the plans.

- Each node takes a list of activities, the index of the activity to use,
the index of the plan to add to and whether to add to the afternoon or
morning slot.
- If the activity index reaches the end of the list, all activities have
been added successfully and the plan is returned.
- Otherwise the activity is attempted to be added into the plan in either
the specified afternoon or morning. If the desired action cannot be taken, no
branching happens and *Optional.empty()* is immediately returned.
- If successful, the method is recursively called, for the afternoon or
morning slot for each plan with the same incremented activity index.
- If any of the branches are successful (*Optional.isPresent()* is returned)
the return value is immediately returned, ending any branching.
- If none of the branches were successful, Optional.empty() is returned.
-In worst case, the time complexity is O((2P)^A), as we will have
fully searched a tree with a depth equal to the number of Activities,
branching 2 * number of Plans each time. However, in most cases there will be
many possible solutions so a full search isn't required and we return and
stop branching as soon as a solution is found. Ordering the activities in
descending order and preferring adding first to the afternoon (as it is an
hour longer so more flexibility) should generally lead us towards solutions
faster.


My output from provided sample activities.xml
--------------

    Team 1:
    09:00 am : 2-wheeled Segways 45min
    09:45 am : Indiano Drizzle 45min
    10:30 am : Human Table Football 30min
    11:00 am : Giant Puzzle Dinosaurs 30min
    11:30 am : New Zealand Haka 30min
    12:00 pm : Lunch Break 60min
    01:00 pm : Duck Herding 60min
    02:00 pm : Viking Axe Throwing 60min
    03:00 pm : Cricket 2020 60min
    04:00 pm : Monti Carlo or Bust 60min
    05:00 pm : Staff Motivation Presentation
    
    Team 2:
    09:05 am : Enigma Challenge 45min
    09:50 am : Learning Magic Tricks 40min
    10:30 am : Buggy Driving 30min
    11:00 am : Arduino Bonanza 30min
    11:30 am : Wine Tasting sprint
    11:45 am : Time Tracker sprint
    12:00 pm : Lunch Break 60min
    01:00 pm : Laser Clay Shooting 60min
    02:00 pm : Giant Digital Graffiti 60min
    03:00 pm : Digital Tresure Hunt 60min
    04:00 pm : Archery 45min
    04:45 pm : Salsa & Pickles sprint
    05:00 pm : Staff Motivation Presentation
