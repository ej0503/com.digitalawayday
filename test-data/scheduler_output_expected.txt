Team 1:
09:00 am : 2-wheeled Segways 45min
09:45 am : Indiano Drizzle 45min
10:30 am : Human Table Football 30min
11:00 am : Giant Puzzle Dinosaurs 30min
11:30 am : New Zealand Haka 30min
12:00 pm : Lunch Break 60min
01:00 pm : Duck Herding 60min
02:00 pm : Viking Axe Throwing 60min
03:00 pm : Cricket 2020 60min
04:00 pm : Monti Carlo or Bust 60min
05:00 pm : Staff Motivation Presentation

Team 2:
09:05 am : Enigma Challenge 45min
09:50 am : Learning Magic Tricks 40min
10:30 am : Buggy Driving 30min
11:00 am : Arduino Bonanza 30min
11:30 am : Wine Tasting sprint
11:45 am : Time Tracker sprint
12:00 pm : Lunch Break 60min
01:00 pm : Laser Clay Shooting 60min
02:00 pm : Giant Digital Graffiti 60min
03:00 pm : Digital Tresure Hunt 60min
04:00 pm : Archery 45min
04:45 pm : Salsa & Pickles sprint
05:00 pm : Staff Motivation Presentation

